# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os
import requests
import json
import shutil
import xlsxwriter
import pandas as pd
import time
from tkinter import *
from datetime import timedelta, date

# yourpath = 'D:/Picture/2021'
# year = '2021'
yourpath = ''
year = ''
root = Tk()
root.title("Picture Generatetor")
ExcelData = []


def getStaff():
    staffData = requests.get(
        "https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/staff/?limit=99999")
    return staffData


def execPicData(path, year):
    ExcelData = []
    yourpath = path
    year = year
    count = 0
    command = "start cmd /K "
    os.chdir(yourpath)
    if os.path.isfile('RecriptData.xlsx'):
        print("File exist")
        os.remove('RecriptData.xlsx')
        print("File remove! Process 1")
    # staffData = getStaff()

    # lastIndex = len(staffData.json()) - 1

    for root, dirs, files in os.walk(yourpath, topdown=False):
        for filename in files:
            filename = filename.split('_')
            filename = filename[0].split('.')
            resData = requests.get(
                "https://medppss.medicine.psu.ac.th/api/dome/PP3S/carpark/member_old/year/" + year + "/uid/" + filename[
                    0])
            if len(resData.json()['data']) > 0:
                resData2 = requests.post(
                    "https://medppss.medicine.psu.ac.th/api/dome/PP3S/carpark/member_old/id/" + str(
                        resData.json()['data'][0]['id']),
                    json={
                        'data': {
                            'process_status': '1',
                            'update_by': '43654'
                        }
                    })
                if resData2.json()['status']:
                    count += 1
                    print(str(count) + ') รหัส ' + filename[0] + ' อัพเดทสเตตัสจ่ายเงินสำเร็จ')
                    ExcelData.append({
                        'ลำดับ': str(count),
                        'รหัส': filename[0],
                        'สถานะ': 'Success',
                    })

                else:
                    count += 1
                    print(str(count) + ') รหัส ' + filename[0] + ' อัพเดทสเตตัสจ่ายเงินไม่สำเร็จ !!')
                    ExcelData.append({
                        'ลำดับ': str(count),
                        'รหัส': filename[0],
                        'สถานะ': 'Not Success',
                    })
            else:
                count += 1
                print(str(count) + ') รหัส ' + filename[0] + ' ไม่พบรหัส !!')
                ExcelData.append({
                    'ลำดับ': str(count),
                    'รหัส': filename[0],
                    'สถานะ': 'ไม่พบรหัส',
                })


        # สร้าง DataFrame ที่มี 1 คอลัมน์ชื่อ 'Data'
    dataframe = pd.DataFrame.from_records([s for s in ExcelData])

    # สร้าง Pandas Excel Writer เพื่อใช้เขียนไฟล์ Excel โดยใช้ Engine เป็น xlsxwriter
    # โดยตั้งชื่อไฟล์ว่า 'simple_data.xlsx'
    writer = pd.ExcelWriter('RecriptData.xlsx')

    # นำข้อมูลที่สร้างไว้ในตัวแปร dataframe เขียนลงไฟล์
    dataframe.to_excel(writer, sheet_name='หน้าที่1', encoding='utf8')

    # # จบการทำงาน Pandas Excel writer และเซฟข้อมูลออกมาเป็นไฟล์ Excel
    try:
        writer.save()
        print("File Save! Success")
    except:
        if os.path.isfile('RecriptData.xlsx'):
            print("File exist")
            os.remove('RecriptData.xlsx')
            print("File Removed!")
            writer.save()
            print("File Save!")
        else:
            writer.save()
            print("File Save!")
    else:
        print("File Error")


def print_hi(root):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def generateUI():
    Label(root, text="ระบบประมวลผลใบเสร็จ", fg="red", font=20).place(x=150, y=0)
    Label(root, text="ระบุ Path", fg="red", font=20).place(x=100, y=50)
    Label(root, text="ระบุ ปี", fg="red", font=20).place(x=240, y=50)

    # btnExec = Button(root, text="ประมวลผล",bg="blue", fg="white", command=execPicData).pack()

    txtPath = StringVar()
    Entry(root, textvariable=txtPath).place(x=80, y=80)

    txtPath2 = StringVar()
    Entry(root, textvariable=txtPath2).place(x=220, y=80)

    def ExecData():
        PathData = txtPath.get()
        YearData = txtPath2.get()
        execPicData(PathData, YearData)

    btnExec = Button(root, text="ประมวลผล", bg="blue", fg="white", command=ExecData).place(x=180, y=120)

    # btnExec = Button(root, text="ประมวลผล",bg="blue", fg="white", command=GetPath).pack()

    root.geometry("400x200")
    root.resizable(False, False)
    root.mainloop()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # execPicData()
    generateUI()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
